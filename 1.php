<?php

use clases\ejercicios1\{
    Oficio,
    Persona,
    Fechas
};

require_once './autoload.php';
?>

<?php

$persona1 = new Persona([
    "nombre" => "Ramon"
        ]);

$persona2 = new Persona([
    "nombre" => "Ramon"
        ]);
var_dump($persona1);
var_dump($persona2);
var_dump(Persona::$numero);

$oficio1 = new Oficio([
    "nombre" => "Panadero",
    "salarioBase" => 1000,
        ]);

var_dump($oficio1);

$trabajo1 = new clases\ejercicios1\Trabajan($persona1, $oficio1, Fechas::hoy());

var_dump($trabajo1);
