<?php

require_once "autoload.php";

use clases\ejercicios2\Cliente;

$cliente1 = new Cliente(23, 0);
var_dump($cliente1);

// $persona = new Base(); // al ser una clase abstracta no se puede instanciar
//$direccion = new Direccion(); // al ser un trait no soporta instancias

$empleado1 = new \clases\ejercicios2\Empleado(50);
var_dump($empleado1);
echo $empleado1->presentacion();
echo $empleado1->mostrarInformacion();
