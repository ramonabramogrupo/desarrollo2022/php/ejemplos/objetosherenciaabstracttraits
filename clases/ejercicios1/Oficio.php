<?php

namespace clases\ejercicios1;

class Oficio {

    public $nombre;
    public $salarioBase;
    public $horasSemanales;

    public function __construct($argumentos = []) {
        $valoresPorDefecto = [
            "nombre" => "",
            "salarioBase" => 800,
            "horasSemanales" => 40
        ];

        $datos = array_merge($argumentos, array_diff_key($valoresPorDefecto, $argumentos));
        extract($datos);
        $this->nombre = $nombre;
        $this->salarioBase = $salarioBase;
        $this->horasSemanales = $horasSemanales;
    }

}
