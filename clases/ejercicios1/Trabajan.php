<?php

namespace clases\ejercicios1;

class Trabajan {

    public $persona;
    public $trabajo;
    public $fechaComienzo;
    public $fechaFin;

    public function __construct(Persona $persona, Oficio $trabajo, string $fechaComienzo = null, string $fechaFin = null) {
        $this->persona = clone $persona;
        $this->trabajo = clone $trabajo;
        $this->fechaComienzo = $fechaComienzo;
        $this->fechaFin = $fechaFin;
    }

}
