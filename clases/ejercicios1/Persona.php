<?php

namespace clases\ejercicios1;

class Persona {

    public static $numero = 0;
    public $nombre;
    public $sexo;
    public $fecha;

    public function __construct($datos = []) {
        extract($datos);
        $this->nombre = isset($nombre) ? $nombre : "";
        $this->sexo = $sexo ?? "";
        $this->fecha = $fecha ?? "";
        self::$numero++;
    }

    public function getNombre(): string {
        return $this->nombre;
    }

    public function getSexo(): string {
        return $this->sexo;
    }

    public function getFecha(): string {
        return $this->fecha;
    }

    public function setNombre(string $nombre): self {
        $this->nombre = $nombre;
        return $this;
    }

    public function setSexo(string $sexo): self {
        $this->sexo = $sexo;
        return $this;
    }

    public function setFecha(string $fecha): self {
        $this->fecha = $fecha;
        return $this;
    }

}
