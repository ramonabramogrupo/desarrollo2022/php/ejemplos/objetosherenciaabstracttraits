<?php

namespace clases\ejercicios2;

class Cliente extends Base {

    use Direccion;

    public $codigo;
    public $gastos;

    public function getCodigo() {
        return $this->codigo;
    }

    public function getGastos() {
        return $this->gastos;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
        return $this;
    }

    public function setGastos($gastos) {
        $this->gastos = $gastos;
        return $this;
    }

    public function presentacion() {
        return "hola";
    }

    public function hablar() {
        return "Buenas";
    }

    public function __construct($codigo, $gastos) {
        $this->codigo = $codigo;
        $this->gastos = $gastos;
        parent::__construct();
    }

}
