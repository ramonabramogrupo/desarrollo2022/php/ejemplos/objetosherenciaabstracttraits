<?php

namespace clases\ejercicios2;

interface Trabajador {

    public function calcularSueldo();

    public function mostrarInformacion();
}
