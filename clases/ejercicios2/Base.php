<?php

namespace clases\ejercicios2;

abstract class Base {

    public $nombre;
    public $fechaNacimiento;
    public $apellidos;

    public function getNombre() {
        return $this->nombre;
    }

    public function getFechaNacimiento() {
        return $this->fechaNacimiento;
    }

    public function getApellidos() {
        return $this->apellidos;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
        return $this;
    }

    public function setFechaNacimiento($fechaNacimiento) {
        $this->fechaNacimiento = $fechaNacimiento;
        return $this;
    }

    public function setApellidos($apellidos) {
        $this->apellidos = $apellidos;
        return $this;
    }

    abstract public function presentacion();

    public function hablar() {
        return "hola";
    }

    public function __construct($nombre = "", $fechaNacimiento = "", $apellidos = "") {
        $this->nombre = $nombre;
        $this->fechaNacimiento = $fechaNacimiento;
        $this->apellidos = $apellidos;
    }

}
